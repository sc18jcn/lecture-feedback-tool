from django.shortcuts import render, redirect
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib import messages
from django.views.generic.edit import FormMixin
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse
from django.http import HttpResponseForbidden, JsonResponse
from django.db.models import Q

from .forms import PostForm, JoinLectureForm
from .models import Lecture, Post, Emotion
from teachers.models import Teacher
from students.models import Student

# Create your views here.

def Home(request): # Redirects user to the lectures page if logged in
    if not logged_in(request):
        return redirect('students-login')
    return redirect('main-lectures')

# Class based view to list all lectures associated with user
class LectureListView(UserPassesTestMixin, ListView):
    model = Lecture
    template_name = 'main/lectures.html'
    context_object_name = 'lectures'
    paginate_by = 4

    def test_func(self): # Checks USER is logged in
        if not logged_in(self.request):
            return False
        return True
        
    def get_queryset(self):
        user = get_user(self.request)
        if self.request.session['status'] == 1: # if teacher is logged in, method queries lecture model for lectures that the teacher has created
            queryset = Lecture.objects.filter(owner=user).order_by('-date')
        elif self.request.session['status'] == 2: # if student is logged in, method queries lecture model for lectures that the student has joined
            queryset = Lecture.objects.filter(students=user).order_by('-date')
        return queryset

    def get_context_data(self, **kwargs): # Sends user and user type to template
        context = super(LectureListView, self).get_context_data(**kwargs)
        context['user'] = get_user(self.request)
        context['status'] = self.request.session['status']
        return context

# Class based view to handle an individual lecture
# Handles the lecture, lecture chat, lecture emotions
class LectureDetailView(FormMixin, UserPassesTestMixin, DetailView):
    model = Lecture
    template_name = 'main/lecture.html'
    context_object_name = 'lecture'
    form_class = PostForm

    def test_func(self): # Checks USER is logged in, AND USER has access to the lecture
        if not logged_in(self.request):
            return False
        user = get_user(self.request)
        lecture = self.get_object()
        if user.lecture_set.filter(module_code=lecture.module_code).exists():
            return True
        return False

    def get_success_url(self):
        return reverse('main-lecture', kwargs={'pk': self.object.id})

    def get_context_data(self, **kwargs): # Sends user, user type, PostForm, lectures posts and emotions to the template
        context = super(LectureDetailView, self).get_context_data(**kwargs)
        lecture = self.object
        user = get_user(self.request)
        context['user'] = user
        context['status'] = self.request.session['status']
        context['form'] = PostForm()
        context['posts'] = lecture.post_set.order_by('-date')
        
        if self.request.session['status'] == 1: # If teacher is logged in, total for each current emotion is sent to template
            context['good_count'] = Emotion.objects.filter(rating=1, lecture=lecture, emotion_level=lecture.emotion_level).count()
            context['middle_count'] = Emotion.objects.filter(rating=2, lecture=lecture, emotion_level=lecture.emotion_level).count()
            context['bad_count'] = Emotion.objects.filter(rating=3, lecture=lecture, emotion_level=lecture.emotion_level).count()
        elif self.request.session['status'] == 2: # If student is logged in, the current emotion they have selected is sent to template
            emotion = Emotion.objects.filter(student=user, lecture=lecture, emotion_level=lecture.emotion_level).first()
            if emotion:
                context['rating'] = emotion.rating
        return context

    def post(self, request, *args, **kwargs): # Handles the templates POST requests
        lecture = Lecture.objects.filter(id=request.resolver_match.kwargs['pk']).first()
        user = get_user(self.request)
        try: # Checks to see if "reset" button was pressed by teacher
            if request.POST['reset']:
                lecture.emotion_level += 1 # Increases emotion level (saves all the previous votes)
                lecture.save()
                students = lecture.students.all()
                for student in students: # Sets all the students votes back to not voted
                    emotion = Emotion.objects.create(rating=0, student=student, lecture=lecture, emotion_level=lecture.emotion_level)
                    emotion.save()
                return redirect('main-lecture', lecture.id)
        except:
            pass

        try: # Checks to see if student voted for an emotion
            if request.POST['emotion']:
                if Emotion.objects.filter(student=user, lecture=lecture, emotion_level=lecture.emotion_level).exists():
                    emotion = Emotion.objects.filter(student=user, lecture=lecture, emotion_level=lecture.emotion_level).first()
                    emotion.rating = request.POST['emotion'] # 1-Good, 2-Middle, 3-Bad
                    emotion.save()
                return redirect('main-lecture', lecture.id)
        except:
            pass

        self.object = self.get_object() # Retrives form from student
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form): # Overrides the valid function to add the author of the post to the form before saving
        user = get_user(self.request)
        obj = form.save(commit=False) 
        
        obj.author = user
        obj.lecture = self.object
        obj.save()
        return super(LectureDetailView, self).form_valid(form)

# Class based view to handle creating lectures
class LectureCreateView(UserPassesTestMixin, CreateView):
    model = Lecture
    template_name = 'main/createLecture.html'
    fields = ['module_code','lecture_title','lecture_description']

    def test_func(self): # Checks TEACHER is logged in
        if not logged_in(self.request):
            return False
        if self.request.session['status'] == 2:
            return False
        return True

    def form_valid(self, form): # Overrides valid function the set the lecture owner to the current teacher
        user = get_user(self.request)
        form.instance.owner = user
        return super().form_valid(form)

    def get_context_data(self, **kwargs): # Sends the user and user type to template
        context = super(LectureCreateView, self).get_context_data(**kwargs)
        context['user'] = get_user(self.request)
        context['status'] = self.request.session['status']
        return context

# Class based view to handle updating lectures
class LectureUpdateView(UserPassesTestMixin, UpdateView):
    model = Lecture
    template_name = 'main/updateLecture.html'
    fields = ['module_code','lecture_title','lecture_description']

    def test_func(self): # Checks TEACHER is logged in, AND lecture is owned by current teacher
        if not logged_in(self.request):
            return False
        if self.request.session['status'] == 2:
            return False
        user = get_user(self.request)
        lecture = self.get_object()
        if user == lecture.owner:
            return True
        return False
    
    def get_context_data(self, **kwargs): # Sends user and user type to template
        context = super(LectureUpdateView, self).get_context_data(**kwargs)
        context['user'] = get_user(self.request)
        context['status'] = self.request.session['status']
        return context

# Class based view to handle the list of students in current lecture
class ListStudentsView(UserPassesTestMixin, DetailView):
    model = Lecture
    template_name = 'main/listStudents.html'

    def test_func(self): # Checks USER is logged in
        if not logged_in(self.request):
            return False
        user = get_user(self.request)
        lecture = self.get_object()
        if self.request.session['status'] == 1: # If teacher is logged in, checks they own the lecture
            if lecture.owner == user:
                return True
        elif self.request.session['status'] == 2: # If student is logged in, checks they have joined the lecture
            if user.lecture_set.filter(module_code=lecture.module_code).exists():
                return True
        return False

    def get_context_data(self, **kwargs): # Sends user, user type and list of students in lecture to the template
        context = super(ListStudentsView, self).get_context_data(**kwargs)
        user = get_user(self.request)
        lecture = self.get_object()
        students = lecture.students.all()
        context['students'] = students
        context['user'] = user
        context['status'] = self.request.session['status']
        return context

# Function based view to handle viewing student profiles
def viewProfile(request, pk, student_id):
    if not logged_in(request): # Checks USER is logged in
        return HttpResponseForbidden()
    user = get_user(request)
    lecture = Lecture.objects.get(id=pk)
    if request.session['status'] == 1: # If teacher is logged in, checks they own the lecture
        if lecture.owner != user:
            return HttpResponseForbidden()
    elif request.session['status'] == 2: # If student is logged in, checks they have joined the lecture
        if not user.lecture_set.filter(module_code=lecture.module_code).exists():
            return HttpResponseForbidden()
    student = Student.objects.get(id=student_id)
    context = { # Sends user, user type, lecture and which students profile will be viewed to the template
        'user':user,
        'student':student,
        'lecture':lecture,
        'status':request.session['status']
    }
    return render(request, 'main/viewProfile.html', context)

# Function based view to handle removing student from lecture
def kickStudent(request, pk, student_id):
    if not logged_in(request): 
        return HttpResponseForbidden()
    if request.session['status'] == 2: # Checks TEACHER is logged in
        return HttpResponseForbidden()
    user = get_user(request)
    lecture = Lecture.objects.get(id=pk)
    if lecture.owner != user: # Checks teacher owns the lecture
        return HttpResponseForbidden()

    student = Student.objects.get(id=student_id)
    if lecture.owner == user:
        lecture.students.remove(student)
    return redirect('main-students', pk)

# Class based view to handle deleting lectures
class LectureDeleteView(UserPassesTestMixin, DeleteView):
    model = Lecture
    template_name = 'main/deleteLecture.html'
    success_url = '/lectures/'

    def test_func(self): # Checks TEACHER is logged in, AND lecture is owned by current teacher
        if not logged_in(self.request):
            return False
        if self.request.session['status'] == 2:
            return False
        user = get_user(self.request)
        lecture = self.get_object()
        if user == lecture.owner:
            return True
        return False

    def get_context_data(self, **kwargs): # Sends the user and user type to template
        context = super(LectureDeleteView, self).get_context_data(**kwargs)
        context['user'] = get_user(self.request)
        context['status'] = self.request.session['status']
        return context

# Class based view to handle the emotion statistics for a lecture
class LectureStatsView(UserPassesTestMixin, DetailView):
    model = Lecture
    template_name = 'main/lectureStats.html'

    def test_func(self): # Checks TEACHER is logged in, AND lecture is owned by current teacher
        if not logged_in(self.request):
            return False
        if self.request.session['status'] == 2:
            return False
        user = get_user(self.request)
        lecture = self.get_object()
        if user == lecture.owner:
            return True
        return False

    def get_context_data(self, **kwargs): # Sends user and user type to template
        lecture = self.object
        user = get_user(self.request)
        context = super(LectureStatsView, self).get_context_data(**kwargs)
        context['user'] = user
        context['status'] = self.request.session['status']

        # Sends count of each emotion to template
        context['good_count'] = Emotion.objects.filter(rating=1, lecture=lecture, emotion_level=lecture.emotion_level).count()
        context['middle_count'] = Emotion.objects.filter(rating=2, lecture=lecture, emotion_level=lecture.emotion_level).count()
        context['bad_count'] = Emotion.objects.filter(rating=3, lecture=lecture, emotion_level=lecture.emotion_level).count()
        context['none_count'] = Emotion.objects.filter(rating=0, lecture=lecture, emotion_level=lecture.emotion_level).count()

        # Sends total number of students voted and total number of students not voted to template
        context['total_votes'] = Emotion.objects.filter(~Q(rating=0), lecture=lecture, emotion_level=lecture.emotion_level).count()
        context['total_students'] = lecture.students.all().count()

        # Sends a dictionary of each student in the lecture, along with their current emotion and their previous emotions
        students = lecture.students.all()
        context['student_data'] = []
        for student in students:
            context['student_data'].append([
                student,
                Emotion.objects.filter(lecture=lecture, student=student, emotion_level=lecture.emotion_level).first().rating,
                Emotion.objects.filter(lecture=lecture, student=student, rating=1).count(),
                Emotion.objects.filter(lecture=lecture, student=student, rating=2).count(),
                Emotion.objects.filter(lecture=lecture, student=student, rating=3).count(),
                Emotion.objects.filter(lecture=lecture, student=student, rating=0).count(),
            ])
        return context

# Function based view to obtain data used for the lecture statistics charts
def get_chart_data(request, pk):
    lecture = Lecture.objects.get(id=pk)

    # Count of each emotion
    good_count = Emotion.objects.filter(rating=1, lecture=lecture, emotion_level=lecture.emotion_level).count()
    middle_count = Emotion.objects.filter(rating=2, lecture=lecture, emotion_level=lecture.emotion_level).count()
    bad_count = Emotion.objects.filter(rating=3, lecture=lecture, emotion_level=lecture.emotion_level).count()
    none_count = Emotion.objects.filter(rating=0, lecture=lecture, emotion_level=lecture.emotion_level).count()

    labels = ['Good','Middle','Bad','None']
    data = [good_count,middle_count,bad_count,none_count]

    # Cumulative count of each emotion
    total_good = Emotion.objects.filter(rating=1, lecture=lecture).count()
    total_middle = Emotion.objects.filter(rating=2, lecture=lecture).count()
    total_bad = Emotion.objects.filter(rating=3, lecture=lecture).count()

    data2 = [total_good, total_middle, total_bad]
    
    return JsonResponse(data={ # Sends data to charts as Json
        'labels': labels,
        'data': data,
        'data2':data2,
    })

# Function based view to handle joining lectures
def join_lecture(request):
    if not logged_in(request): 
        return HttpResponseForbidden()
    if request.session['status'] == 1: # Checks STUDENT is logged in
        return HttpResponseForbidden()

    user = get_user(request)
    if request.method == 'POST':
        form = JoinLectureForm(request.POST)
        if form.is_valid():
            if user.lecture_set.filter(module_code=form.cleaned_data['module_code']).exists(): # Checks to see if they are already joined the lecture
                messages.add_message(request, messages.ERROR, 'You are already in that lecture')
                return redirect('main-join')
                
            lecture = Lecture.objects.filter(module_code=form.cleaned_data['module_code']).first()
            lecture.students.add(user)
            if not Emotion.objects.filter(student=user, lecture=lecture).exists(): # Adds the initial vote (Not voted) to the emotion model if it doesnt already exist
                emotion = Emotion.objects.create(rating=0, student=user, lecture=lecture, emotion_level=lecture.emotion_level)
                emotion.save()  
            messages.success(request, f'Successfully joined the lecture')
            return redirect('main-lecture', lecture.id)
    else:
        form = JoinLectureForm()

    context = { # Sends form and user to template
        'form': form,
        'user': user,
    }
    return render(request, 'main/joinLecture.html', context)

# Function based view to handle leaving lectures
def leave_lecture(request, pk):
    if not logged_in(request):
        return HttpResponseForbidden()
    if request.session['status'] == 1: # Checks STUDENT is logged in
        return HttpResponseForbidden()
    user = get_user(request)
    lecture = Lecture.objects.get(id=pk)
    if not user.lecture_set.filter(module_code=lecture.module_code).exists(): # Checks current student is in the lecture
            return HttpResponseForbidden()
    lecture.students.remove(user)
    return redirect('main-lectures')

# Function based view to handle deleting posts
def delete_chat(request, pk, post_id):
    if not logged_in(request):
        return HttpResponseForbidden()
    if request.session['status'] == 2: # Checks TEACHER is logged in
        return HttpResponseForbidden()
    user = get_user(request)
    lecture = Lecture.objects.get(id=pk)
    if lecture.owner != user: # Checks lecture is owned by current teacher
        return HttpResponseForbidden()
    post = Post.objects.get(id=post_id)
    post.delete()
    return redirect('main-lecture', pk)


# The following functions are not views

# Function to return the current user
def get_user(request):
    if request.session['status'] == 1: # If teacher is logged in, teacher model is queried to return teacher
        return Teacher.objects.filter(id=request.session['user']).first()
    elif request.session['status'] == 2: # If student is logged in, student model is queried to return student
        return Student.objects.filter(id=request.session['user']).first()

# Function to check if a user is logged in
def logged_in(request):
    try:
        if request.session['status'] == 0:
            return False
        return True
    except: 
        return False