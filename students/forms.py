from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from .models import Student
from teachers.models import Teacher

class StudentRegisterForm(UserCreationForm):
    first_name = forms.CharField(required=True)
    last_name = forms. CharField(required=True)
    email = forms.EmailField(required=True)
    username = forms.CharField(required=True)

    class Meta:
        model = Student
        fields = ['username','first_name','last_name','email']

    def clean_email(self):
        email = self.cleaned_data['email']
        if Teacher.objects.filter(email=email).exists() or Student.objects.filter(email=email).exists():
            raise ValidationError("Email already in use")
        return email 

    def clean_username(self):
        username = self.cleaned_data['username']
        if Teacher.objects.filter(username=username).exists() or Student.objects.filter(username=username).exists():
            raise ValidationError("This username has been taken")
        return username 

class StudentLoginForm(forms.ModelForm):
    username = forms.CharField(required=True)
    password = forms.CharField(required=True, widget=forms.PasswordInput)

    class Meta:
        model = Student
        fields = ['username', 'password']

    def clean(self): # Checks username and password match
        form = self.cleaned_data
        username = self.cleaned_data['username']
        student = Student.objects.filter(username=username).first()
        if student and student.password == form.get("password"):
            return form
        else:
            raise ValidationError("Username or password incorrect")

class StudentUpdateForm(forms.ModelForm):

    class Meta:
        model = Student
        fields = ['username','first_name','last_name','email','image']

    def clean_email(self): # Checks email is not being in either user model
        email = self.cleaned_data['email']
        if 'email' in self.changed_data:
            if Teacher.objects.filter(email=email).exists() or Student.objects.filter(email=email).exists():
                raise ValidationError("Email already in use")
        return email 

    def clean_username(self): # Checks username is not being used in either user model
        username = self.cleaned_data['username']
        if 'username' in self.changed_data:
            if Teacher.objects.filter(username=username).exists() or Student.objects.filter(username=username).exists():
                raise ValidationError("This username has been taken")
        return username 