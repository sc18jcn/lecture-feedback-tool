from django.db import models
from django.utils import timezone
from django.urls import reverse

from teachers.models import Teacher
from students.models import Student

import string, random, time

# Generates random 8 character code
def id_generator(size=8, chars=string.ascii_lowercase + string.digits):
    random.seed(time.process_time())
    return ''.join(random.choice(chars) for _ in range(size))

# Create your models here.

class Lecture(models.Model):
    module_code = models.CharField(max_length=10, unique=True)
    lecture_title = models.CharField(max_length=20)
    lecture_description = models.TextField()
    date = models.DateTimeField(default=timezone.now)
    passcode = models.CharField(max_length=8, default=id_generator())
    owner = models.ForeignKey('teachers.Teacher', on_delete=models.CASCADE) # OneToMany relationship as lecture can only be owned by one teacher but teacher can own many lectures
    students = models.ManyToManyField('students.Student') # ManyToMany relationship as lecture can have many students, and students can be in many lectures
    emotion_level = models.IntegerField(default=0)

    def __str__(self):
        return f'({self.id}) {self.module_code} : {self.owner.username}'

    def get_absolute_url(self):
        return reverse('main-lecture', kwargs={'pk': self.pk})

class Post(models.Model):
    content = models.CharField(max_length=100)
    date = models.DateTimeField(default=timezone.now)
    lecture = models.ForeignKey('Lecture', on_delete=models.CASCADE) # OneToMany relationship as a post can only be in one lecture but lectures can have many posts
    author = models.ForeignKey('students.Student', on_delete=models.CASCADE) # OneToMany relationship as a post can only be owned by one student but students can have many posts
    anonymous = models.BooleanField()

    def __str__(self):
        return f'({self.id}) {self.lecture.module_code} : {self.author.username} - {self.content}'

class Emotion(models.Model):
    rating = models.IntegerField() #0-None 1-Good 2-Middle 3-Bad
    student = models.ForeignKey('students.Student', on_delete=models.CASCADE) # OneToMany relationship as an emotion can only be owned by one student, but a student can have many emotions
    lecture = models.ForeignKey('Lecture', on_delete=models.CASCADE) # OneToMany relationship as an emotion can only be associated with one lecture, but a lecture can have many emotions
    emotion_level = models.IntegerField()

    def __str__(self):
        return f'({self.id}) {self.lecture.module_code} : {self.student.username} - Level:{self.emotion_level} - {self.rating}'

