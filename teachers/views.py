from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.hashers import make_password, check_password

from main.views import logged_in, get_user
from .forms import TeacherRegisterForm, TeacherLoginForm, TeacherUpdateForm
from .models import Teacher

# Create your views here.
def register(request):
    if logged_in(request): # Redirects user to home page if logged in
        return redirect('main-lectures')
        
    if request.method == 'POST':
        form = TeacherRegisterForm(request.POST)
        if form.is_valid():
            password = make_password(form.cleaned_data['password1'])
            t1 = Teacher.objects.create(username=form.cleaned_data['username'], first_name=form.cleaned_data['first_name'], last_name=form.cleaned_data['last_name'], email=form.cleaned_data['email'], password=password)
            t1.save()
            messages.success(request, f'Your account has been created!')
            return redirect('teachers-login')
    else:
        form = TeacherRegisterForm()
    return render(request, 'teachers/register.html', {'form': form})

def login(request):
    if logged_in(request): # Redirects user to home page if logged in
        return redirect('main-lectures')

    if request.method =='POST':
        form = TeacherLoginForm(request.POST)
        teacher = Teacher.objects.filter(username=form.data['username']).first()
        if teacher and check_password(form.data['password'], teacher.password):
            request.session['status'] = 1 # 0-Logged out, 1-Teacher, 2-Student
            request.session['user'] = teacher.id # Custom sessions to keep track of what type of user is logged in
            return redirect('main-lectures')
    else:
        form = TeacherLoginForm()
    return render(request, 'teachers/login.html', {'form': form})

def logout(request):
    if not logged_in(request): # Redirects user to login page if not logged in
        return redirect('teachers-login')

    request.session['status'] = 0
    request.session['user'] = 0
    return render(request, 'teachers/logout.html')

def profile(request):
    if not logged_in(request): # Redirects user to login page if not logged in
        return redirect('students-login')
    if request.session['status'] == 2: # Redirects to student profile if student is logged in
        return redirect('students-profile')

    user = get_user(request) 
    if request.method == 'POST':
        form = TeacherUpdateForm(request.POST, request.FILES, instance=user)
        if form.changed_data: # Checks to see if form has been updated before validating
            if form.is_valid():
                form.save()
                messages.success(request, f'Your profile has been updated!')
                return redirect('teachers-profile')
    else:
        form = TeacherUpdateForm(instance=user)

    context = {
        'form': form,
        'user': user,
        'status': 1,
    }
    return render(request, 'teachers/profile.html', context)

