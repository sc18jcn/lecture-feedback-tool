from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.hashers import make_password, check_password

from .forms import StudentRegisterForm, StudentLoginForm, StudentUpdateForm
from .models import Student
from main.views import logged_in, get_user

# Create your views here.
def register(request):
    if logged_in(request): # Redirects user to home page if logged in
        return redirect('main-lectures')

    if request.method == 'POST':
        form = StudentRegisterForm(request.POST)
        if form.is_valid():
            password = make_password(form.cleaned_data['password1'])
            s1 = Student.objects.create(username=form.cleaned_data['username'], first_name=form.cleaned_data['first_name'], last_name=form.cleaned_data['last_name'], email=form.cleaned_data['email'], password=password)
            s1.save()
            messages.success(request, f'Your account has been created!')
            return redirect('students-login')
    else:
        form = StudentRegisterForm()
    return render(request, 'students/register.html', {'form': form})

def login(request):
    if logged_in(request): # Redirects user to home page if logged in
        return redirect('main-lectures')

    if request.method =='POST':
        form = StudentLoginForm(request.POST)
        student = Student.objects.filter(username=form.data['username']).first()
        if student and check_password(form.data['password'], student.password):
            request.session['status'] = 2 # 0-Logged out, 1-Teacher, 2-Student
            request.session['user'] = student.id # Custom sessions to keep track of what type of user is logged in
            return redirect('main-lectures')
    else:
        form = StudentLoginForm()
    return render(request, 'students/login.html', {'form': form})

def logout(request):
    if not logged_in(request): # Redirects user to login page if not logged in
        return redirect(students-login)
    request.session['status'] = 0 
    request.session['user'] = 0
    return render(request, 'students/logout.html')
    
def profile(request):
    if not logged_in(request): # Redirects user to login page if not logged in
        return redirect('students-login')
    if request.session['status'] == 1: # Redirects to teacher profile if teacher is logged in
        return redirect('teachers-profile')

    user = get_user(request)
    if request.method == 'POST':
        form = StudentUpdateForm(request.POST, request.FILES, instance=user)
        if form.changed_data: # Checks to see if form has been updated before validating
            if form.is_valid():
                form.save()
                messages.success(request, f'Your profile has been updated!')
                return redirect('students-profile')
    else:
        form = StudentUpdateForm(instance=user)

    context = {
        'form': form,
        'user': user,
        'status': 2,
    }
    return render(request, 'students/profile.html', context)

