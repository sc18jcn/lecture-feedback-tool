from django.contrib import admin
from .models import Lecture, Post, Emotion

# Register your models here.
admin.site.register(Lecture)
admin.site.register(Post)
admin.site.register(Emotion)