This guide assumes you have the following installed:

1. Python 3.7
2. Pip 21.0.1

To run program on your own machine:

1. Clone repository to your computer
2. Create virtual environment (HIGHLY RECOMMENDED)
3. Change directory into project repository ($ cd project/)
4. Install requirements.txt ($ pip3 install -r requirements.txt)
5. Run program ($ python3 manage.py runserver)
6. Visit localhost:8000 in your browser to view site


*** Extra Information ***

To view models via the admin page, you can create a superuser using the following command in the terminal: 
- $ python3 manage.py createsuperuser

You can then enter a username and password which you will use as the login credentials for the admin page. The admin page can be found at:
- localhost:8000/admin

The code from the git repository contains empty models with no data so you are free to add your own teacher and student accounts, as well as your own lectures, posts, and emotions.

If you wish to view the website with models already populated with data, please visit: https://lecturefeedback.pythonanywhere.com/ and use the following guide
(teacher and student have separate login pages):

Login as teacher:
- username: testteacher
- password: testing321


Login as student:
- username: teststudent
- password: testing321

Please use lecture: COMPTEST, to view a lecture populated with students, posts, emotions already done for you.

