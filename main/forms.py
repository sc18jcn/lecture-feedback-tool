from django import forms
from django.core.exceptions import ValidationError
from .models import Post, Lecture

class PostForm(forms.ModelForm):
    content = forms.CharField(widget=forms.Textarea(attrs={'rows':1, 'class':'form-control form-control-lg', 'placeholder':'Ask question...'}))
    anonymous = forms.BooleanField(required=False, label="Hide username from other students")

    class Meta:
        model = Post
        fields = ['anonymous','content']

class JoinLectureForm(forms.ModelForm):
    module_code = forms.CharField(required=True)
    passcode = forms.CharField(required=True)

    class Meta:
        model = Lecture
        fields = ['module_code', 'passcode']

    def clean(self): # Checks that module code and passcode are a match in the lecture model
        form = self.cleaned_data
        module_code = self.cleaned_data['module_code']
        lecture = Lecture.objects.filter(module_code=module_code).first()
        if lecture and lecture.passcode == form.get("passcode"):
            return form
        else:
            raise ValidationError("Lecture credentials are incorrect")
