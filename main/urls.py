from django.urls import path
from .views import Home, LectureListView, LectureDetailView, LectureCreateView, LectureUpdateView, ListStudentsView, viewProfile, kickStudent, LectureDeleteView, LectureStatsView, get_chart_data, join_lecture, leave_lecture, delete_chat
from . import views

urlpatterns = [
    path('', Home),
    path('home/', Home),
    path('lectures/', LectureListView.as_view(), name='main-lectures'),
    path('lectures/<int:pk>/', LectureDetailView.as_view(), name='main-lecture'),
    path('lectures/new/', LectureCreateView.as_view(), name='main-create'),
    path('lectures/<int:pk>/update/', LectureUpdateView.as_view(), name='main-update'),
    path('lectures/<int:pk>/students/', ListStudentsView.as_view(), name='main-students'),
    path('lectures/<int:pk>/profile/<int:student_id>/', viewProfile, name='main-profile'),
    path('lectures/<int:pk>/profile/<int:student_id>/kick/', kickStudent, name='main-kick'),
    path('lectures/<int:pk>/delete/', LectureDeleteView.as_view(), name='main-delete'),
    path('lectures/<int:pk>/stats/', LectureStatsView.as_view(), name='main-stats'),
    path('lectures/<int:pk>/stats/charts/', get_chart_data, name='charts'),
    path('lectures/join/', join_lecture, name='main-join'),
    path('lectures/<int:pk>/leave/', leave_lecture, name='main-leave'),
    path('lectures/<int:pk>/deletechat/<int:post_id>/', delete_chat, name='main-delete-chat'),
]
