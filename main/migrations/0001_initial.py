# Generated by Django 3.1.7 on 2021-03-29 14:45

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('students', '0001_initial'),
        ('teachers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Lecture',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('module_code', models.CharField(max_length=10, unique=True)),
                ('lecture_title', models.CharField(max_length=20)),
                ('lecture_description', models.TextField()),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('passcode', models.CharField(default='KWfoYnSN', max_length=8)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='teachers.teacher')),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', models.TextField()),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='students.student')),
                ('lecture', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.lecture')),
            ],
        ),
    ]
