from django.db import models
from PIL import Image
from django.utils import timezone

# Create your models here.

class Student(models.Model):
    username = models.CharField(max_length=20)
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    email = models.EmailField()
    password = models.CharField(max_length=100)
    image = models.ImageField(default='default.png', upload_to='profile_pics')
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = "uid"

    def __str__(self):
        return f'({self.id}) {self.username}'

    def save(self, **kwargs): # Down sizes any image uploaded to save storage
        super().save()
        img = Image.open(self.image.path)
        if img.height > 300 or img.width > 300:
            output_size = (300,300)
            img.thumbnail(output_size)
            img.save(self.image.path)